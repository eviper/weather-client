<div class="card">
    <h5 class="card-header">Get weather by date</h5>
    <div class="card-body">
        <form action="{{route('date')}}" id="get-weather-by-date" method="post">
            <div class="mb-3">
                <label for="dateInput" class="form-label">Date</label>
                <input type="text" name="date" class="form-control" id="dateInput" placeholder="{{ \Carbon\Carbon::now()->toDateString() }}">
            </div>
            <button type="submit" class="btn btn-primary ri">get!</button>
        </form>
    </div>
    <div class="card-footer" id="get-weather-footer">

    </div>
</div>

@push('js')
    <script>
        window.addEventListener('load', function () {
            const form = document.getElementById('get-weather-by-date');
            form.addEventListener('submit', getWeather);
        });

        let cardFooter = document.getElementById('get-weather-footer');
        let getWeather = (e) => {
            e.preventDefault();
            let form = e.currentTarget;
            let action = form.getAttribute('action');
            axios
                .post(action, new URLSearchParams(new FormData(form)).toString())
                .then((response) => {
                    let result = response.data.result;
                    cardFooter.innerHTML = 'Weather: ' + result;
                })
                .catch((error) => {
                    let response = error.response;
                    cardFooter.innerHTML = 'Error: ' + response.data.errors.date;
                });
        };
    </script>
@endpush
