<div class="card">
    <h5 class="card-header">Recent weather:</h5>
    <div class="card-body">
        <table class="table table-striped table-sm">
            <thead class="table-dark">
            <tr>
                <th scope="col">Date</th>
                <th scope="col">Weather</th>
            </tr>
            </thead>
            <tbody id="table-weather-history">
            </tbody>
        </table>
    </div>
</div>


@push('js')
    <script>
        window.addEventListener('load', function() {
            const table = document.getElementById('table-weather-history');
            axios.post('{{ route('history') }}', {
                     lastDays: 30,
                 })
                 .then((response) => {
                     let result = response.data.result;
                     for (let date in result) {
                         if (result.hasOwnProperty(date)) {
                             let row = {
                                 date: date,
                                 temp: result[date]
                             };
                             table.append(makeTr(row));
                         }
                     }
                 })
                 .catch((response) => console.log(response.response));
        });

        let makeTr = function (row) {
            let tr = document.createElement('tr');
            tr.append(makeTd(row.date));
            tr.append(makeTd(row.temp));

            return tr;
        };

        let makeTd = function (item) {
            let td = document.createElement('td');
            td.innerHTML = item;
            return td;
        };
    </script>
@endpush

