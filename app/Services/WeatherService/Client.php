<?php

namespace App\Services\WeatherService;


use GuzzleHttp\Client as Gazzle;
use GuzzleHttp\RequestOptions;

class Client
{
    const JSON_RPC_VERSION = '2.0';

    const METHOD_URI = '/';

    protected Gazzle $client;

    public function __construct(string $url)
    {
        $this->client = new Gazzle([
            'headers'  => ['Content-Type' => 'application/json'],
            'base_uri' => $url,
        ]);
    }

    public function send(string $method, array $params): array
    {
        $response = $this->client
            ->post(self::METHOD_URI, [
                RequestOptions::JSON => [
                    'jsonrpc' => self::JSON_RPC_VERSION,
                    'id'      => time(),
                    'method'  => $method,
                    'params'  => $params,
                ],
            ])->getBody()->getContents();

        return json_decode($response, true);
    }
}
