<?php

namespace App\Services\WeatherService;


class WeatherService
{
    private $config;

    public function __construct()
    {
        $this->config = config('services.weather_service');
    }

    public function getByDate(string $date): array
    {
        $url = $this->getUrl();

        return (new Client($url))->send(
            'weather.getByDate',
            [
                'date' => $date,
            ]
        );
    }

    public function getHistory(int $lastDays): array
    {
        $url = $this->getUrl();

        return (new Client($url))->send(
            'weather.getHistory',
            [
                'lastDays' => $lastDays,
            ]
        );
    }

    private function getUrl()
    {
        if (!isset($this->config['url'])) {
            throw new \RuntimeException('Weather url is not configured!');
        }

        return $this->config['url'];
    }
}
