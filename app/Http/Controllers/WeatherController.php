<?php

namespace App\Http\Controllers;


use App\Http\Requests\Weather\GetByDateRequest;
use App\Http\Requests\Weather\GetHistoryRequest;
use App\Services\WeatherService\WeatherService;
use Illuminate\Http\JsonResponse;

class WeatherController extends Controller
{
    /**
     * @param GetByDateRequest $request
     * @param WeatherService   $service
     *
     * @return JsonResponse
     */
    public function getByDate(GetByDateRequest $request, WeatherService $service): JsonResponse
    {
        $date = $request->get('date');

        return response()->json($service->getByDate($date));
    }

    /**
     * @param GetHistoryRequest $request
     * @param WeatherService    $service
     *
     * @return JsonResponse
     */
    public function getHistory(GetHistoryRequest $request, WeatherService $service): JsonResponse
    {
        $lastDays = $request->get('lastDays');

        return response()->json($service->getHistory($lastDays));
    }
}
