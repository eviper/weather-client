<?php

namespace App\Http\Requests\Weather;


use Illuminate\Foundation\Http\FormRequest;

class GetByDateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'date' => 'required|date_format:Y-m-d',
        ];
    }

    public function messages(): array
    {
        return [
            'date.required' => 'Date required!',
            'date.date_format' => 'Invalid date format!',
        ];
    }
}
